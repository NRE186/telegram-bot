import requests
import settings


class Weather:

    def __init__(self):
        self.city = settings.city
        self.city_id = settings.city_id
        self.appid = settings.api_key

    def get_weather(self):
        try:
            res = requests.get("http://api.openweathermap.org/data/2.5/weather",
                               params={'id': self.city_id, 'units': 'metric', 'lang': 'ru', 'APPID': self.appid})
            data = res.json()
            return data
        except Exception as e:
            print("Exception (weather):", e)
            pass

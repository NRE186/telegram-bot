# coding=utf-8

import os
import time
import datetime
import calendar
import telebot
from multiprocessing import Process

import settings
from keyboards import ButtonsText, Keyboards
from weather import Weather


"""Настройка времени"""
os.environ["TZ"] = "Asia/Tashkent"
time.tzset()


bot = telebot.TeleBot(settings.token)

"""Инициализация основных классов для работы клавиатуры"""
text = ButtonsText()
keyboard = Keyboards()

"""Инициализация класса для работы с погодой"""
weather = Weather()


def check_send_messages():
    """Отправка сообщений в определенные дни и время"""
    my_date = datetime.date.today()
    while True:
        if (
            str(datetime.datetime.now().time())[:5] == "07:00"
            and calendar.day_name[my_date.weekday()] == "Tuesday"
        ):
            for id in settings.ids:
                bot.send_message(id, "Расписание на вторник")
                bot.send_message(
                    id,
                    "08:00 : Сети ЭВМ, У708(Павлов С.И) \n09:40 : Сети ЭВМ, У606(Павлов С.И) / Мультимедиа технологии, А417(Фёдоров Д.А) \n11:20 : Мультимедиа технологии, А417(Фёдоров Д.А) / Сети ЭВМ, У606(Павлов С.И) \n13:20 : Мультимедиа технологии, А417(Фёдоров Д.А)",
                )
        elif (
            str(datetime.datetime.now().time())[:5] == "07:50"
            and calendar.day_name[my_date.weekday()] == "Tuesday"
        ):
            for id in settings.ids:
                bot.send_message(id, "Расписание на вторник")
                bot.send_message(
                    id,
                    "Сети ЭВМ, У708(Павлов С.И)",
                )
        elif (
            str(datetime.datetime.now().time())[:5] == "09:30"
            and calendar.day_name[my_date.weekday()] == "Tuesday"
        ):
            for id in settings.ids:
                bot.send_message(id, "Расписание на вторник")
                bot.send_message(
                    id,
                    "Сети ЭВМ, У606(Павлов С.И) / Мультимедиа технологии, А417(Фёдоров Д.А)",
                )
        elif (
            str(datetime.datetime.now().time())[:5] == "11:10"
            and calendar.day_name[my_date.weekday()] == "Tuesday"
        ):
            for id in settings.ids:
                bot.send_message(id, "Расписание на вторник")
                bot.send_message(
                    id,
                    "Мультимедиа технологии, А417(Фёдоров Д.А) / Сети ЭВМ, У606(Павлов С.И)",
                )
        elif (
            str(datetime.datetime.now().time())[:5] == "12:50"
            and calendar.day_name[my_date.weekday()] == "Tuesday"
        ):
            for id in settings.ids:
                bot.send_message(id, "Расписание на вторник")
                bot.send_message(
                    id,
                    "Мультимедиа технологии, А417(Фёдоров Д.А)",
                )
        time.sleep(60)


def send_weather(chat_id):
    """Функция для отправки погоды ботом"""
    weather_data = weather.get_weather()
    bot.send_message(chat_id, "⛅ Погода в Сургуте")
    bot.send_message(chat_id, f"🌡️ ️Температура:  {weather_data['main']['temp']}°")
    bot.send_message(
        chat_id, f"🌡️️ Минимальная температура: {weather_data['main']['temp_min']}°"
    )
    bot.send_message(
        chat_id, f"🌡️ Максимальная температура: {weather_data['main']['temp_max']}°"
    )
    bot.send_message(chat_id, f"👌️️ Ощущается: {weather_data['main']['feels_like']}°")
    bot.send_message(
        chat_id, f"☁️Погодные условия: {weather_data['weather'][0]['description']}"
    )


@bot.message_handler(commands=["start"])
def start_message(message):
    bot.delete_message(message.chat.id, message.message_id)
    bot.send_message(
        message.chat.id,
        "Здраствуй, ты начал общение со мной",
        reply_markup=keyboard.main_keyboard,
    )
    bot.send_sticker(
        message.chat.id,
        "CAACAgIAAxkBAAL_mF7-EUF8bhk0E3CFjZCzViGQB0MwAAISAAOX0wUNejCJB9KHAWAaBA",
    )


@bot.message_handler(content_types=["text"])
def send_text(message):
    if message.text == text.hi_text:
        bot.send_message(message.chat.id, "Привет")
    elif message.text == text.back_text:
        bot.delete_message(message.chat.id, message.message_id)
        bot.send_message(
            message.chat.id, text.back_text, reply_markup=keyboard.main_keyboard
        )
        bot.delete_message(message.chat.id, message.message_id + 1)
    elif message.text == text.timetable_text:
        bot.delete_message(message.chat.id, message.message_id)
        keyboard = telebot.types.InlineKeyboardMarkup()
        key_monday = telebot.types.InlineKeyboardButton(
            text="Понедельник", callback_data="monday"
        )
        keyboard.add(key_monday)
        key_tuesday = telebot.types.InlineKeyboardButton(
            text="Вторник", callback_data="tuesday"
        )
        keyboard.add(key_tuesday)
        key_thursday = telebot.types.InlineKeyboardButton(
            text="Четверг", callback_data="thursday"
        )
        keyboard.add(key_thursday)
        key_friday = telebot.types.InlineKeyboardButton(
            text="Пятница", callback_data="friday"
        )
        keyboard.add(key_friday)
        bot.send_message(
            message.from_user.id, text="Выбери день недели", reply_markup=keyboard
        )
    elif message.text == text.timebells_text:
        bot.delete_message(message.chat.id, message.message_id)
        bot.send_message(message.chat.id, "Расписание звонков")
        bot.send_message(
            message.chat.id,
            "1 пара : 08.00 - 09.30 \n2 пара : 09.40 - 11.10 \n3 пара : 11.20 - 12.50 \n4 пара : 13.20 - 14.50 \n5 пара : 15.00 - 16.30 \n6 пара : 16.40 - 18.10 \n7 пара : 18.20 - 19.50 \n8 пара : 20.00 - 21.30",
        )
    elif message.text == text.weather_text:
        bot.delete_message(message.chat.id, message.message_id)
        send_weather(message.chat.id)


# Обработчик нажатий на кнопки
@bot.callback_query_handler(func=lambda call: True)
def callback_worker(call):
    bot.delete_message(call.message.chat.id, call.message.message_id)
    if call.data == "monday":
        bot.send_message(call.message.chat.id, "Расписание на понедельник:")
        bot.send_message(
            call.message.chat.id,
            "08:00 : Администрирование в ИС У708(Заикин П.В) \n09:40 : Методы оптимизации, У606(Шайторова И.А) / ООП, У601(Павлов С.И) \n11:20 : ООП, У601(Павлов С.И) / Методы оптимизации, У606(Шайторова И.А) \n13:20 : ООП, У505(Павлов С.И)",
        )
    elif call.data == "tuesday":
        bot.send_message(call.message.chat.id, "Расписание на вторник:")
        bot.send_message(
            call.message.chat.id,
            "08:00 : Сети ЭВМ, У708(Павлов С.И) \n09:40 : Сети ЭВМ, У606(Павлов С.И) / Мультимедиа технологии, А417(Фёдоров Д.А) \n11:20 : Мультимедиа технологии, А417(Фёдоров Д.А) / Сети ЭВМ, У606(Павлов С.И) \n13:20 : Мультимедиа технологии, А417(Фёдоров Д.А)",
        )
    elif call.data == "thursday":
        bot.send_message(call.message.chat.id, "Расписание на четверг:")
        bot.send_message(
            call.message.chat.id,
            "08:00 : Методы оптимизации, У903(Шайторова И.А) \n09:40 : Администрирование в ИС, У606(Берестин Д.К) / Инт. системы и технологии, У607(Шайторова И.А) \n11:20 : Инт. системы и технологии, У607(Шайторова И.А) / Администрирование в ИС, У606(Берестин Д.К) \n13:20 : Инт. системы и технологии, У506(Шайторова И.А)",
        )
    elif call.data == "friday":
        bot.send_message(call.message.chat.id, "Расписание на пятницу:")
        bot.send_message(
            call.message.chat.id,
            "13:20 : Комп. графика, У704(Быковских Д.А) \n15:00 : Комп. графика, У705(Быковских Д.А) \n16:30 : Комп. графика, У705(Быковских Д.А)",
        )


if __name__ == "__main__":
    p1 = Process(target=check_send_messages, args=())
    p1.start()
    while True:
        try:
            bot.polling(none_stop=True)
        except Exception as e:
            print(e)
            time.sleep(15)

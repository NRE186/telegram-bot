import telebot


class ButtonsText:
    """Текст с кнопок"""

    back_text = "↩ Назад"
    hi_text = "👋 Привет"
    weather_text = "⛅ Информация о погоде"
    timetable_text = "⏲ Расписание"
    timebells_text = "⌚ Расписание звонков"


class Buttons:
    def __init__(self):
        self.buttons_text = ButtonsText()
        """Кнопки клавиатуры"""
        self.back = telebot.types.KeyboardButton(self.buttons_text.back_text)
        """Основная клавиатура"""
        self.hi = telebot.types.KeyboardButton(self.buttons_text.hi_text)
        self.weather = telebot.types.KeyboardButton(self.buttons_text.weather_text)
        self.timetable = telebot.types.KeyboardButton(self.buttons_text.timetable_text)
        self.timebells = telebot.types.KeyboardButton(self.buttons_text.timebells_text)


class Keyboards:
    def __init__(self):
        self.buttons = Buttons()
        self.main_keyboard = telebot.types.ReplyKeyboardMarkup(
            resize_keyboard=True, row_width=2
        )
        self.main_keyboard.add(
            self.buttons.hi,
            self.buttons.weather,
            self.buttons.timetable,
            self.buttons.timebells,
        )
